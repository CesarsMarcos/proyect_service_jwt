package com.example.jwt.message.response;

public class ApiResponse {

	private Boolean success;
	private String message;
	private Object objecto;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObjecto() {
		return objecto;
	}

	public void setObjecto(Object objecto) {
		this.objecto = objecto;
	}

}
