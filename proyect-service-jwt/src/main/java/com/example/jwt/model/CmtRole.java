package com.example.jwt.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name= "CMT_ROLE")
public class CmtRole  implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_ROLE")
	private Long cod;
	
	@Column(unique=true,name="DES_ROLE")
	private String desRole;

	public Long getCod() {
		return cod;
	}

	public void setCod(Long cod) {
		this.cod = cod;
	}

	public String getDesRole() {
		return desRole;
	}

	public void setDesRole(String desRole) {
		this.desRole = desRole;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
