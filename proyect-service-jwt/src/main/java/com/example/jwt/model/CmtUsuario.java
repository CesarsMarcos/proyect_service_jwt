package com.example.jwt.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
@Table(name = "CMT_USUARIO")
public class CmtUsuario implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_USUARIO")
	private Long codUsuario;

	@Column(unique = true, name = "DES_USUARIO")
	private String desUsuario;

	@Column(name = "DES_PASSWORD")
	private String desPassword;

	@Column(name = "IND_BAJA")
	private String indBaja;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FEC_REGISTRO")
	@JsonFormat(pattern = "dd-MM-yyyy", shape = Shape.STRING)
	private Date fecRegistro;

	@Column(name = "USU_REGISTRO")
	private String usuRegistro;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "SST_USUARIO_ROLE", joinColumns = @JoinColumn(name = "USUARIO_ID"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
	private List<CmtRole> roles;

	public Long getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Long codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getDesUsuario() {
		return desUsuario;
	}

	public void setDesUsuario(String desUsuario) {
		this.desUsuario = desUsuario;
	}

	public String getIndBaja() {
		return indBaja;
	}

	public void setIndBaja(String indBaja) {
		this.indBaja = indBaja;
	}

	public List<CmtRole> getRoles() {
		return roles;
	}

	public void setRoles(List<CmtRole> roles) {
		this.roles = roles;
	}

	public Date getFecRegistro() {
		return fecRegistro;
	}

	public void setFecRegistro(Date fecRegistro) {
		this.fecRegistro = fecRegistro;
	}

	public String getUsuRegistro() {
		return usuRegistro;
	}

	public void setUsuRegistro(String usuRegistro) {
		this.usuRegistro = usuRegistro;
	}

	public String getDesPassword() {
		return desPassword;
	}

	public void setDesPassword(String desPassword) {
		this.desPassword = desPassword;
	}

	public CmtUsuario() {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
