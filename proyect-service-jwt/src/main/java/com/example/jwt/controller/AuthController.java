package com.example.jwt.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.jwt.message.request.LoginRequest;
import com.example.jwt.message.response.JwtAuthenticationResponse;
import com.example.jwt.security.jwt.JwtTokenProvider;

@RestController
@RequestMapping("/api/auth/")
public class AuthController {

	@Autowired
	AuthenticationManager autenticationManager;
	
	@Autowired
	JwtTokenProvider tokenProvider;
	
	@PostMapping("signin")
	public ResponseEntity<?> authenticate (@Valid @RequestBody LoginRequest loginRequest){
		
		Authentication authentication = autenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						loginRequest.getUsername(),
						loginRequest.getPassword()));
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		String jwt= tokenProvider.generateToken(authentication);
		
		return new ResponseEntity<JwtAuthenticationResponse>(new JwtAuthenticationResponse(jwt),HttpStatus.OK);
	}

}
