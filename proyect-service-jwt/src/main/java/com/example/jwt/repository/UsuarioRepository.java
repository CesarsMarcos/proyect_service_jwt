package com.example.jwt.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jwt.model.CmtUsuario;

@Repository
public interface UsuarioRepository extends JpaRepository<CmtUsuario,Serializable> {

	CmtUsuario findByDesUsuario(String username);

}
