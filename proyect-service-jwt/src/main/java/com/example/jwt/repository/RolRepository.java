package com.example.jwt.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jwt.model.CmtRole;

@Repository
public interface RolRepository  extends JpaRepository<CmtRole, Serializable>{

}
