package com.example.jwt.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.jwt.model.CmtUsuario;
import com.example.jwt.repository.UsuarioRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private static final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsService.class);

	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		CmtUsuario usuario = usuarioRepository.findByDesUsuario(username);
		if (usuario == null) {
			LOG.error("El usuario no se encuentra registrado " + username);
			throw new UsernameNotFoundException("El usuario no se encuentra registrado " + username);
		}
		return UserPrincipal.create(usuario);
	}


}
