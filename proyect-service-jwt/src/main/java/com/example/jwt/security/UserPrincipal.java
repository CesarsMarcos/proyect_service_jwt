package com.example.jwt.security;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.jwt.model.CmtUsuario;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserPrincipal implements UserDetails {

	private Long codUsuario;
	private String desUsuario;
	@JsonIgnore
	private String desPassword;
	private Collection<? extends GrantedAuthority> authorities;

	public UserPrincipal(Long codUsuario, String desUsuario, String desPassword,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.codUsuario = codUsuario;
		this.desUsuario = desUsuario;
		this.desPassword = desPassword;
		this.authorities = authorities;
	}

	public static UserPrincipal create(CmtUsuario usuario) {

		List<GrantedAuthority> authorities = usuario.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getDesRole())).collect(Collectors.toList());

		return new UserPrincipal(usuario.getCodUsuario(), usuario.getDesUsuario(), usuario.getDesPassword(),
				authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return desPassword;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return desUsuario;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public Long getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Long codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getDesUsuario() {
		return desUsuario;
	}

	public void setDesUsuario(String desUsuario) {
		this.desUsuario = desUsuario;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 9218449784979767498L;
}
